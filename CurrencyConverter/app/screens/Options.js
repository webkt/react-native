import React, { Component } from 'react';
import propTypes from 'prop-types';
import {
  ScrollView, StatusBar, Platform, Linking,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { ListItem, Separator } from '../components/List';

const ICON_PREFIX = Platform.OS === 'ios' ? 'ios' : 'md';
const ICON_COLOR = '#868686';
const ICON_SIZE = 23;

class Options extends Component {
  static propTypes = {
    navigation: propTypes.oneOfType(propTypes.object).isRequired,
  };

  handleThemePress = () => {
    // eslint-disable-next-line react/destructuring-assignment
    this.props.navigation.navigate('Themes', { title: 'Themes' });
  };

  handleSitePress = () => {
    // eslint-disable-next-line no-alert
    Linking.openURL('http://fixer.io').catch(() => alert('Fixer.io cant be opened right now.'));
  };

  render() {
    return (
      <ScrollView>
        <StatusBar translucent={false} barStyle="default" />
        <ListItem
          text="Themes"
          onPress={this.handleThemePress}
          customIcon={
            // eslint-disable-next-line react/jsx-wrap-multilines
            <Ionicons
              name={`${ICON_PREFIX}-arrow-forward`}
              color={ICON_COLOR}
              size={ICON_SIZE}
            />
          }
        />
        <Separator />
        <ListItem
          text="Site"
          onPress={this.handleSitePress}
          customIcon={
            // eslint-disable-next-line react/jsx-wrap-multilines
            <Ionicons
              name={`${ICON_PREFIX}-link`}
              color={ICON_COLOR}
              size={ICON_SIZE}
            />
          }
        />
      </ScrollView>
    );
  }
}

export default Options;
