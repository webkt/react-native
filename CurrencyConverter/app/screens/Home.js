/* eslint-disable react/destructuring-assignment */
import React, { Component } from 'react';
import propTypes from 'prop-types';

import { StatusBar, KeyboardAvoidingView } from 'react-native';

import { connect } from 'react-redux';
import { Container } from '../components/Container';
import { Logo } from '../components/Logo';
import { InputWithButton } from '../components/TextInput';
import { ClearButton } from '../components/Button';
import { LastConverted } from '../components/Text';
import { Header } from '../components/Header';

import {
  swapCurrency,
  changeCurrencyAmount,
  getInitialConversion,
} from '../actions/currencies';

class Home extends Component {
  static propTypes = {
    navigation: propTypes.oneOfType(propTypes.object).isRequired,
    dispatch: propTypes.oneOfType(propTypes.func).isRequired,
    baseCurrency: propTypes.oneOfType(propTypes.string).isRequired,
    quoteCurrency: propTypes.oneOfType(propTypes.string).isRequired,
    amount: propTypes.oneOfType(propTypes.number).isRequired,
    conversionRate: propTypes.oneOfType(propTypes.number).isRequired,
    isFetching: propTypes.oneOfType(propTypes.bool).isRequired,
    lastConvertedDate: propTypes.oneOfType(propTypes.object).isRequired,
    primaryColor: propTypes.oneOfType(propTypes.string).isRequired,
    // currencyError: propTypes.oneOfType(propTypes.string).isRequired
  };

  componentWillUnmount() {
    this.props.dispatch(getInitialConversion());
  }

  // componentWillReceiveProps(nextProps) {
  //   const { currencyError, alertWithType } = this.props;
  //   if (nextProps.currencyError && !currencyError) {
  //     alertWithType('error', 'Error', nextProps.currencyError);
  //   }
  // }

  handlePressBaseCurrency = () => {
    this.props.navigation.navigate('CurrencyList', {
      title: 'Base Currency',
      type: 'base',
    });
  };

  handlePressQuoteCurrency = () => {
    this.props.navigation.navigate('CurrencyList', {
      title: 'Quote Currency',
      type: 'quote',
    });
  };

  handleTextChange = (text) => {
    this.props.dispatch(changeCurrencyAmount(text));
  };

  handleSwapCurrency = () => {
    this.props.dispatch(swapCurrency());
  };

  handleOptionsPress = () => {
    this.props.navigation.navigate('Options', { title: 'Options' });
  };

  render() {
    let quotePrice = '...';
    if (!this.props.isFetching) {
      quotePrice = (this.props.amount * this.props.conversionRate).toFixed(2);
    }

    return (
      <Container backgroundColor={this.props.primaryColor}>
        <StatusBar translucent={false} barStyle="light-content" />
        <Header onPress={this.handleOptionsPress} />
        <KeyboardAvoidingView behavior="padding">
          <Logo tintColor={this.props.primaryColor} />
          <InputWithButton
            buttonText={this.props.baseCurrency}
            onPress={this.handlePressBaseCurrency}
            defaultValue={this.props.amount.toString()}
            keyboardType="numeric"
            onChangeText={this.handleTextChange}
            textColor={this.props.primaryColor}
          />
          <InputWithButton
            buttonText={this.props.quoteCurrency}
            onPress={this.handlePressQuoteCurrency}
            editable={false}
            value={quotePrice}
            textColor={this.props.primaryColor}
          />
          <LastConverted
            base={this.props.baseCurrency}
            quote={this.props.quoteCurrency}
            conversionRate={this.props.conversionRate}
            date={this.props.lastConvertedDate}
          />
          <ClearButton
            buttonText="Reverse Currencies"
            onPress={this.handleSwapCurrency}
          />
        </KeyboardAvoidingView>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  const { baseCurrency, quoteCurrency, amount } = state.currencies;
  const conversionSelector = state.currencies.conversions[baseCurrency] || {};
  const rates = conversionSelector.rates || {};

  return {
    baseCurrency,
    quoteCurrency,
    amount,
    conversionRate: rates[quoteCurrency] || 0,
    isFetching: conversionSelector.isFetching,
    lastConvertedDate: conversionSelector.date
      ? new Date(conversionSelector.date)
      : new Date(),
    primaryColor: state.themes.primaryColor,
    // currencyError: state.currencies.error
  };
};

export default connect(mapStateToProps)(Home);
