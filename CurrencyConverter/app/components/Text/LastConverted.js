import React from 'react';
import propTypes from 'prop-types';
import moment from 'moment';

import { Text } from 'react-native';
import styles from './styles';

const LastConverted = ({
  base, quote, conversionRate, date,
}) => (
  <Text style={styles.smallText}>
    1
    {' '}
    {base}
    {' '}
    =
    {' '}
    {conversionRate}
    {' '}
    {quote}
    {' '}
    as of
    {' '}
    {moment(date).format('MMMM D, YYYY')}
  </Text>
);

LastConverted.propTypes = {
  date: propTypes.oneOfType(propTypes.object).isRequired,
  base: propTypes.oneOfType(propTypes.string).isRequired,
  quote: propTypes.oneOfType(propTypes.string).isRequired,
  conversionRate: propTypes.oneOfType(propTypes.number).isRequired,
};

export default LastConverted;
