import React from 'react';
import propTypes from 'prop-types';

import { View, Text, TouchableHighlight } from 'react-native';

import styles from './styles';
import Icon from './Icon';

const ListItem = ({
  text,
  onPress,
  selected = false,
  checkMark = true,
  visible = true,
  customIcon = null,
  iconBackground,
}) => (
  <TouchableHighlight onPress={onPress} underlayColor={styles.$underlayColor}>
    <View style={styles.row}>
      <Text style={styles.text}>{text}</Text>
      {selected ? (
        <Icon
          checkMark={checkMark}
          visible={visible}
          iconBackground={iconBackground}
        />
      ) : (
        <Icon />
      )}
      {customIcon}
    </View>
  </TouchableHighlight>
);

ListItem.propTypes = {
  text: propTypes.oneOfType(propTypes.string).isRequired,
  onPress: propTypes.oneOfType(propTypes.func).isRequired,
  selected: propTypes.oneOfType(propTypes.bool).isRequired,
  checkMark: propTypes.oneOfType(propTypes.bool).isRequired,
  visible: propTypes.oneOfType(propTypes.bool).isRequired,
  customIcon: propTypes.oneOfType(propTypes.element).isRequired,
  iconBackground: propTypes.oneOfType(propTypes.string).isRequired,
};

export default ListItem;
