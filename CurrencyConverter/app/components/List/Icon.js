import React from 'react';
import { View, Image } from 'react-native';

import propTypes from 'prop-types';

import styles from './styles';

const Icon = ({ checkMark, visible, iconBackground }) => {
  if (visible) {
    const iconStyles = [styles.icon];
    if (visible) {
      iconStyles.push(styles.iconVisible);
    }

    if (iconBackground) {
      iconStyles.push({ backgroundColor: iconBackground });
    }

    return (
      <View style={iconStyles}>
        {checkMark ? (
          <Image
            style={styles.checkIcon}
            resizeMode="contain"
            source={require('./images/check.png')}
          />
        ) : null}
      </View>
    );
  }
  return <View style={styles.icon} />;
};

Icon.propTypes = {
  checkMark: propTypes.oneOfType(propTypes.bool).isRequired,
  visible: propTypes.oneOfType(propTypes.bool).isRequired,
  iconBackground: propTypes.oneOfType(propTypes.string).isRequired,
};

export default Icon;
