import React from 'react';
import propTypes from 'prop-types';
import {
  View, TouchableHighlight, TextInput, Text,
} from 'react-native';

import color from 'color';

import styles from './styles';

const InputWithButton = (props) => {
  const { onPress, buttonText, editable = true } = props;

  const containerStyles = [styles.container];
  if (editable === false) {
    containerStyles.push(styles.containerDisabled);
  }

  const underlayColor = color(styles.$buttonBackgroundColorBase).darken(
    styles.$buttonBackgroundColorModifier,
  );

  const { textColor } = props;

  const buttonTextStyles = [styles.buttonText];
  if (textColor) {
    buttonTextStyles.push({ color: textColor });
  }

  return (
    <View style={containerStyles}>
      <TouchableHighlight
        underlayColor={underlayColor}
        style={styles.buttonContainer}
        onPress={onPress}
      >
        <Text style={buttonTextStyles}>{buttonText}</Text>
      </TouchableHighlight>
      <View style={styles.border} />
      <TextInput
        style={styles.input}
        underlineColorAndroid="transparent"
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...props}
      />
    </View>
  );
};

InputWithButton.propTypes = {
  onPress: propTypes.oneOfType(propTypes.func).isRequired,
  buttonText: propTypes.oneOfType(propTypes.string).isRequired,
  editable: propTypes.oneOfType(propTypes.bool).isRequired,
  textColor: propTypes.oneOfType(propTypes.string).isRequired,
};

export default InputWithButton;
