import React from 'react';
import propTypes from 'prop-types';
import {
  View, TouchableOpacity, Image, Text,
} from 'react-native';

import styles from './styles';

const ClearButton = ({ buttonText, onPress }) => (
  <TouchableOpacity style={styles.container} onPress={onPress}>
    <View style={styles.wrapper}>
      <Image
        resizeMode="contain"
        style={styles.icon}
        source={require('./images/icon.png')}
      />
      <Text style={styles.text}>{buttonText}</Text>
    </View>
  </TouchableOpacity>
);

ClearButton.propTypes = {
  buttonText: propTypes.oneOfType(propTypes.string).isRequired,
  onPress: propTypes.oneOfType(propTypes.func).isRequired,
};

export default ClearButton;
