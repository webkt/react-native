module.exports = function babelApi(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
  };
};
